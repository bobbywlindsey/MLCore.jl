# MLCore.jl

[![](https://img.shields.io/badge/docs-stable-blue.svg)](https://bobbywlindsey.github.io/MLCore.jl/)

Julia library for machine learning

## Install
```
> Pkg.clone("https://github.com/bobbywlindsey/MLCore.jl.git")
```
