#MLCore.jl Documentation

Julia library for machine learning.

## Using the Package

```
> Pkg.clone("https://github.com/bobbywlindsey/MLCore.jl")
```

## Introduction
```@contents
Pages = [
    "intro/page1.md"
    ]
Depth = 2
```

## Index

```@index
```
