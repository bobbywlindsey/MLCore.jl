using Documenter, MLCore

makedocs(modules=[MLCore],
         doctest=true)

deploydocs(deps = Deps.pip("mkdocs", "python-markdown-math"),
           repo = "github.com/bobbywlindsey/MLCore.jl.git",
           julia  = "0.4.6",
           osname = "linux")
