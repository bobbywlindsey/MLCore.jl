module MLCore
    using
        DataFrames,
        # Distances,
        MLBase

    export
        # types
        KNN,

        # methods
        fit!


    include("knn.jl")
end
