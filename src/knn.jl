type KNN
    # number of neighbors
    k::Int
    weighted_avg::Function
end

function standardize!(df::DataFrames.DataFrame, cols::Array{Int})
    for col in cols
        # use zscore with median and median absolute std to resist outliers
        df[col] = zscore(df[col], median(df[col]), mad(df[col]))
    end
end

function fit!{KNN}(classifier::KNN, data_train::DataFrames.DataFrame,
                   data_test::DataFrames.DataFrame)
    # standardize data
    ncols = size(data_train, 2)
    standardize!(data_train, collect(1:ncols-1))
    standardize!(data_test, collect(1:ncols-1))

    # knn algo here

end
