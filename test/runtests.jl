push!(LOAD_PATH, "../MLCore.jl")

using Base.Test
using MLCore
using MLBase: labelmap, labelencode, sample
using RDatasets: dataset
using Gadfly

function encode_labels(y)
    # map unique labels to unique numbers
    y_map = labelmap(y)
    # encode labels using the above map
    y = labelencode(y_map, y)
end

function split_data(data; train = .7)
    # use sample method in StatsBase when they release support for it
    num_rows = length(data[:, 1])
    data_train_rows = sample(1:num_rows, round(Int, train*num_rows),
                             replace = false)
    data_test_rows = setdiff(collect(1:num_rows), data_train_rows)
    data_train = data[data_train_rows, :]
    data_test = data[data_test_rows, :]
    return data_train, data_test
end

# import iris data set
iris = dataset("datasets", "iris")

# parse features
x = iris[1:end-1]
# parse labels
y = iris[end]
# encode labels
y = encode_labels(y)
# attach encoded labels to x
data = x
data[names(iris)[end]] = y

# split data into training and test sets
data_train, data_test = split_data(data, train = .5)

# define parameters
k = 4
function weighted_avg(k_neighbors::Array)
    1/k_neighbors
end

knn = KNN(k, weighted_avg)
fit!(knn, data_train, data_test)
